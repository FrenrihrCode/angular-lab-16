import {Injectable} from '@angular/core';
import {CompileShallowModuleMetadata} from '@angular/compiler';

@Injectable({providedIn: "root"})
export class HeroesService {
    private heroes: Heroe[] = [
        {
            nombre: "Aquaman",
            bio: "Aquaman es un famoso superhéroe de la Tierra, además del gobernante del reino submarino de Atlántida, siendo a su vez un miembro fundador de la Justice League. Usualmente este personaje se caracteriza por ser un híbrido entre humano y atlante, dándole la capacidad de comunicarse con las criaturas marinas, respirar bajo el agua y nadar a velocidades asombrosas debido a su procedencia atlante.",
            img: "/assets/img/aquaman.png",
            aparicion: "More Fun Action #73 de 1941",
            casa: "DC"
        },
        {
            nombre: "Batman",
            bio: "A diferencia de los superhéroes, no tiene superpoderes: recurre a su intelecto, así como a aplicaciones científicas y tecnológicas para crear armas y herramientas con las cuales lleva a cabo sus actividades. Vive en la mansión Wayne, en cuyos subterráneos se encuentra la Batcueva, el centro de operaciones de Batman. Una gran variedad de villanos conforman la galería de Batman, incluido su archienemigo, el Joker.",
            img: "/assets/img/batman.png",
            aparicion: "El caso del sindicato químico en 1939",
            casa: "DC"
        },
        {
            nombre: "Dardevil",
            bio: "Los orígenes de Daredevil provienen de un accidente que tuvo en su infancia la cual le dio habilidades especiales. Mientras crecía en el vecindario irlandés-estadounidense de Hell's Kitchen de la clase obrera, históricamente áspero y asolado por el crimen, en la ciudad de Nueva York, Matt Murdock es cegado por una sustancia radioactiva que cae de un camión después salvar a un hombre que estaba por ser atropellado por un auto.",
            img: "/assets/img/daredevil.png",
            aparicion: "Daredevil #1 (abril de 1964)",
            casa: "MARVEL"
        },
        {
            nombre: "Hulk",
            bio: "Robert Bruce Banner (hulk) era un científico que trabajaba en una bomba de radiación Gamma para el ejercito estadounidense. Entonces, Bruce se dió cuenta de que un joven llamado Rick Jones había entrado al campo de pruebas. Banner salió corriendo para salvar al muchacho y heroicamente lo empujó a una zanja de protección que le salvó la vida a Rick, pero Bruce quedó expuesto a cantidades extremas de radiación Gamma que alteraron la estructura de su ADN.",
            img: "/assets/img/hulk.png",
            aparicion: "The Incredible Hulk #1 (mayo de 1962)",
            casa: "MARVEL"
        },
        {
            nombre: "Linterna verde",
            bio: "El anillo de poder eligió a Hal Jordan por su habilidad para superar un gran temor. El anillo y sus habilidades le fueron explicados a él, y él heredó el manto de Linterna Verde. Actuando como un agente de policía galáctica, que era su trabajo servir y proteger toda su vida en el Sector 2814. Pronto se daría cuenta de que había otros 3.600 Linternas Verdes de todo el universo, todos supervisados ​​y autorizados por criaturas místicas llamados los Guardianes.",
            img: "/assets/img/linterna-verde.png",
            aparicion: "Showcase #22 (Octubre, 1959)",
            casa: "DC"
        },
        {
            nombre: "Spiderman",
            bio: "Peter Parker era un estudiante de preparatoria que fue mordido por una araña radiactiva en la secundaria. La picadura le llevó a desarrollar poderes similares a los de una araña. Pronto fue capaz de trepar por las paredes y sentir el peligro inminente. Cuando dejó escapar a un ladrón que más tarde asesinó a su tío Ben, Peter aprendió que «Un gran poder conlleva una gran responsabilidad»",
            img: "/assets/img/spiderman.png",
            aparicion: "Amazing Fantasy Vol 1 15 (Agosto de 1962)",
            casa: "MARVEL"
        },
        {
            nombre: "Wolverine",
            bio: "Wolverine fue el tercer mutante conocido en nacer, después de su medio hermano Dientes de Sable y siendo el primero Apocalipsis. Logan es el líder de los X-Men que perdió su memoria por culpa del programa Arma X y desde entonces ha tratado de recuperarla integrando el grupo de los X-Men dirigido por el Profesor Charles Xavier para proteger a la humanidad de los mutantes malignos.",
            img: "/assets/img/wolverine.png",
            aparicion: "The Incredible Hulk (octubre de 1974)",
            casa: "MARVEL"
        }
    ];
    constructor(){
        console.log("Servicio listo")
    }
    getHeroes(): Heroe[]{
        return this.heroes
    }
    getHeroe(idx: string){
        return this.heroes[idx]
    }
}

export interface Heroe {
    nombre: string,
    bio: string,
    img: string,
    aparicion: string,
    casa: string
}